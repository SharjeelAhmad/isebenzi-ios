
//
//  RequestServicesViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 27/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "RequestServicesViewController.h"
#import "SearchResultsTableViewController.h"
@import GMStepper;

@class GMStepper;


@interface RequestServicesViewController ()

@property (weak, nonatomic) IBOutlet GMStepper *stepper;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) UITextField *perDay;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpace;

@end

@implementation RequestServicesViewController {
    NSString *occupation;
    NSString *fee;
    NSDate *dateFrom;
    NSDate *dateTo;
    
    NSString *fromDate;
    NSString *fromTime;
    NSString *fromAm;
    NSString *toTime;
    NSString *toAm;
    NSString *toDate;
    
    MKCoordinateRegion region;
    CLLocationCoordinate2D location;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.stepper.buttonsFont = [UIFont fontWithName:@"Lato-Regular" size:17];
    self.stepper.labelFont = [UIFont fontWithName:@"Lato-Regular" size:17];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    occupation = @"Waiter";
    fee = @"";
    
    dateTo = [NSDate date];
    dateFrom = [NSDate date];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)keyboardDidShow: (NSNotification *) notif{
    NSDictionary *info  = notif.userInfo;
    NSValue *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    self.bottomSpace.constant = 10 + keyboardFrame.size.height;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    self.bottomSpace.constant = 32;
}

- (IBAction)availabilityFrom:(UIButton *)sender {
    [self.perDay resignFirstResponder];
    [_datePickerView setHidden:NO];
    _datePicker.tag = 1;
}

- (IBAction)availabilityTo:(UIButton *)sender {
    [self.perDay resignFirstResponder];
    [_datePickerView setHidden:NO];
    _datePicker.tag = 2;
}

- (IBAction)hideDatePicker:(UIButton *)sender {
    [_datePickerView setHidden:YES];
}

- (IBAction)datePickerChange:(UIDatePicker *)sender forEvent:(UIEvent *)event {
    switch (sender.tag) {
        case 1: {
            dateFrom = sender.date;
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            
            [formatter setDateFormat:@"d/MM/YY"];
            fromDate = [formatter stringFromDate:dateFrom];
            
            [formatter setDateFormat:@"hh꞉mm"];
            fromTime = [formatter stringFromDate:dateFrom];
            
            [formatter setDateFormat:@"a"];
            fromAm = [formatter stringFromDate:dateFrom];
            
            break;
        }
        case 2: {
            dateTo = sender.date;
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            
            [formatter setDateFormat:@"d/MM/YY"];
            toDate = [formatter stringFromDate:dateFrom];
            
            [formatter setDateFormat:@"hh꞉mm"];
            toTime = [formatter stringFromDate:dateFrom];
            
            [formatter setDateFormat:@"a"];
            toAm = [formatter stringFromDate:dateFrom];
            
            break;
        }
        default:
            break;
    }
    
    [self.tableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"cell%d" , (int) indexPath.row]];
    
    if(indexPath.row == 0) {
        RadioButton *waiter = [cell viewWithTag:1];
        RadioButton *handyGardner = [cell viewWithTag:2];
        RadioButton *domestic = [cell viewWithTag:3];
        
        NSArray* buttons = @[waiter , handyGardner , domestic];
        
        [buttons[0] setGroupButtons:buttons]; // Setting buttons into the group
        [buttons[0] setSelected:YES]; // Making the first button initially selected
    }
    
    if(indexPath.row == 2) self.perDay = [cell viewWithTag:1];
    
    if(indexPath.row == 1) {
        UILabel *cfromDate = [cell viewWithTag:1];
        UILabel *cfromTime = [cell viewWithTag:2];
        UILabel *cfromAm = [cell viewWithTag:3];
        UILabel *ctoTime = [cell viewWithTag:5];
        UILabel *ctoAm = [cell viewWithTag:6];
        UILabel *ctoDate = [cell viewWithTag:4];

        cfromDate.text = fromDate ? fromDate : cfromDate.text;
        cfromTime.text = fromTime ? fromTime : cfromTime.text;
        cfromAm.text = fromAm ? fromAm : cfromAm.text;
        
        ctoTime.text = toTime ? toTime : ctoTime.text;
        ctoAm.text = toAm ? toAm : ctoAm.text;
        ctoDate.text = toDate ? toDate : ctoDate.text;
    }
    
    if(indexPath.row == 3) {
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer  alloc] initWithTarget:self action:@selector(longpressToGetLocation:)];
        
        lpgr.minimumPressDuration = 0.5;
        lpgr.delaysTouchesBegan = true;
        lpgr.delegate = self;
        
        MKMapView *map = [cell viewWithTag:1];
        [map addGestureRecognizer:lpgr];
    }
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return 151;
            break;
        case 1:
            return 82;
            break;
        case 2:
            return 44;
            break;
        case 3:
            return 189;
            break;
            
        default:
            return 0;
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return 151;
            break;
        case 1:
            return 82;
            break;
        case 2:
            return 44;
            break;
        case 3:
            return 189;
            break;
            
        default:
            return 0;
            break;
    }
}

- (IBAction)occupationChnage:(RadioButton *)sender {
    if(sender.selected) {
        NSLog(@"Selected color: %@", sender.titleLabel.text);
        occupation = sender.titleLabel.text;
    }
}
- (IBAction)feesChange:(UITextField *)sender {
    fee = sender.text;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    NSLog(@"%f" , mapView.region.center.latitude);
    region = mapView.region;
}


- (IBAction)search:(UIBarButtonItem *)sender {
    if([fee isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Fee required" self:self];
    else if(!dateFrom) [Common showAlertWithTitle:@"Oops" message:@"From date required" self:self];
    else if(!dateTo) [Common showAlertWithTitle:@"Oops" message:@"To date required" self:self];
    else if(location.latitude == 0) [Common showAlertWithTitle:@"Oops" message:@"Map Location required" self:self];
    
    else [self performSegueWithIdentifier:@"search" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SearchResultsTableViewController *results = segue.destinationViewController;
    
    results.occupation = occupation;
    results.persons = [NSString stringWithFormat:@"%f" ,self.stepper.value];
    results.region = location;
    results.perDay = fee;
    
    
    NSDateFormatter *gmtDateFormatter = [[NSDateFormatter alloc] init];
    gmtDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    results.toDate = [gmtDateFormatter stringFromDate:dateTo];
    results.fromDate = [gmtDateFormatter stringFromDate:dateFrom];
}

- (void)longpressToGetLocation:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    MKMapView* view = (MKMapView*) gestureRecognizer.view;
    //CGPoint loc = [gestureRecognizer locationInView:view];
    //MKNewAnnotationContainerView* subview = (MKNewAnnotationContainerView *) [view hitTest:loc withEvent:nil];
    
    CGPoint touchPoint = [gestureRecognizer locationInView:view];
    location = [view convertPoint:touchPoint toCoordinateFromView:view];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location;
    for (id annotation in view.annotations) {
        [view removeAnnotation:annotation];
    }
    [view addAnnotation:point];
}



@end
