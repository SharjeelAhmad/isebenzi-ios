//
//  ParentViewController.m
//  LeoMarchent
//
//  Created by Sharjeel MacBookPro on 22/07/2016.
//  Copyright © 2016 Sharjeel MacBookPro. All rights reserved.
//

#import "ParentViewController.h"
#import "LeftNavViewController.h"
#import "HomeViewController.h"
#import "MyProfileViewController.h"
#import "MyJobsViewController.h"

@interface ParentViewController ()
@property (weak, nonatomic) IBOutlet UINavigationItem *bar;

@end

@implementation ParentViewController

-(void)stylePanel:(UIView *)panel {
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    

    //self.rightPanel = nil;
    
    self.shouldDelegateAutorotateToVisiblePanel = NO;
    //self.leftPanel = [[JALeftViewController alloc] init];

    UINavigationController *centerNavig = [self.storyboard instantiateViewControllerWithIdentifier:@"centerPanel"];
    HomeViewController *center = centerNavig.viewControllers.firstObject;
    center.leftMenuDelegate = self;
    self.centerPanel = centerNavig;
    LeftNavViewController *left = [self.storyboard instantiateViewControllerWithIdentifier:@"leftPanel"];
    left.leftMenuDelegate = self;
    
    self.leftPanel = left;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)toggleLeftMenu {
    [self toggleLeftPanel:self];
}

-(void)refreshLeftScreen {
    [self.leftPanel viewWillAppear:NO];
}

-(void)toggleHomeScreen {
    UINavigationController *centerNavig = [self.storyboard instantiateViewControllerWithIdentifier:@"centerPanel"];
    HomeViewController *center = centerNavig.viewControllers.firstObject;
    center.leftMenuDelegate = self;
    self.centerPanel = centerNavig;
}
//
-(void)toggleHomeScreenWithArray:(NSArray*)recipes {
    UINavigationController *centerNavig = [self.storyboard instantiateViewControllerWithIdentifier:@"centerPanel"];
    HomeViewController *center = centerNavig.viewControllers.firstObject;
    center.leftMenuDelegate = self;
    self.centerPanel = centerNavig;
}
//
-(void)toggleProfileScreen {
    UINavigationController *centerNavig = [self.storyboard instantiateViewControllerWithIdentifier:@"profile"];
    MyProfileViewController *center = centerNavig.viewControllers.firstObject;
    center.leftMenuDelegate = self;
    self.centerPanel = centerNavig;
}
//
//
-(void)toggleJobsScreen {
    UINavigationController *navig = [self.storyboard instantiateViewControllerWithIdentifier:@"jobs"];
    MyJobsViewController  *center = navig.viewControllers.firstObject;
    center.leftMenuDelegate = self;
    self.centerPanel = navig;
}

-(void)logout {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:USER_LOGIN];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"start"] animated:YES completion:^{
        
    }];
}


//
//-(void)toggleAboutScreen {
//    UINavigationController *navig = [self.storyboard instantiateViewControllerWithIdentifier:@"contact"];
//    ContactUsViewController  *center = navig.viewControllers.firstObject;
//    center.leftMenuDelegate = self;
//    self.centerPanel = navig;
//}
//
//-(void)toggleInsertScreen {
//    UINavigationController *navig = [self.storyboard instantiateViewControllerWithIdentifier:@"insert"];
//    InsertRecpieViewController  *center = navig.viewControllers.firstObject;
//    center.leftMenuDelegate = self;
//    self.centerPanel = navig;
//}
//
//-(void)toggleFavouriteScreen {
//    UINavigationController *navig = [self.storyboard instantiateViewControllerWithIdentifier:@"favourite"];
//    FavouriteViewController  *center = navig.viewControllers.firstObject;
//    center.leftMenuDelegate = self;
//    self.centerPanel = navig;
//}
//
//-(void)toggleProductScreen {
//    UINavigationController *navig = [self.storyboard instantiateViewControllerWithIdentifier:@"product"];
//    ProductViewController  *center = navig.viewControllers.firstObject;
//    center.leftMenuDelegate = self;
//    self.centerPanel = navig;
//}

@end
