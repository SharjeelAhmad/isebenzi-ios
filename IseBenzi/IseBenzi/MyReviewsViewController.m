//
//  MyReviewsViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 16/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "MyReviewsViewController.h"

@interface MyReviewsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *dataSource;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *rating;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end

@implementation MyReviewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)viewWillAppear:(BOOL)animated {
    [Server getRatingsOnSuccess:^(NSArray *array) {
        self.dataSource = array;
        [self.tableView reloadData];
    } onFailure:^(NSString *error) {
        
    }];
    
    self.rating.value = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_AVERAGE_RATINGS].doubleValue;
    
      self.userName.text = [NSString stringWithFormat:@"%@ %@" , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_F_NAME] , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_L_NAME]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *name = [cell viewWithTag:1];
    UILabel *comment = [cell viewWithTag:2];
    HCSStarRatingView *stars = [cell viewWithTag:3];
    
    name.text = self.dataSource[indexPath.row][@"seeker"];
    comment.text = self.dataSource[indexPath.row][@"comment"];
    stars.value = [NSString stringWithFormat:@"%@" , self.dataSource[indexPath.row][@"rating"]].doubleValue;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

@end
