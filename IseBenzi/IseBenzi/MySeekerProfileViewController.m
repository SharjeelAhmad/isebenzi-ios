//
//  MySeekerProfileViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 07/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "MySeekerProfileViewController.h"

@interface MySeekerProfileViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *requestServices;
@property (weak, nonatomic) IBOutlet UIButton *statusReport;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

@end

@implementation MySeekerProfileViewController {
    NSArray *staticLabels;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [Common setNavigationTitleFont:self.navigationController];
    staticLabels = @[@"Personal Information", @"Manage Locations"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
    
    [self performSelectorInBackground:@selector(getImageFromURL:) withObject:[NSString stringWithFormat:@"%@%@" , SERVER_URL , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_IMAGE]]];
}

-(void) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.userImageView.image = result;
    });
}

-(void)viewWillAppear:(BOOL)animated {
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height /2;
    self.userImageView.layer.masksToBounds = YES;
    self.userImageView.layer.borderWidth = 0;
}

- (IBAction)logout:(UIBarButtonItem *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:USER_LOGIN];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"start"] animated:YES completion:^{
        
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return staticLabels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = [staticLabels objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"profile" sender:self];
    }
    
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"locations" sender:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)requestServices:(UIButton *)sender {
}

- (IBAction)uploadProfilePic:(UIButton *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction* openPhotoLibary = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectPhoto:YES];
    }];
    
    UIAlertAction* openCamera = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectPhoto:NO];
    }];
    
    //[openCamera setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    //[openPhotoLibary setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    
    [alertController addAction:openPhotoLibary];
    [alertController addAction:openCamera];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)selectPhoto:(BOOL)camera {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if(camera) picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    else picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        //Or you can get the image url from AssetsLibrary
        //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
        
        self.userImageView.image = image;
        
        [Server uploadPhoto:UIImageJPEGRepresentation(image, 0.6) onProgressUpdate:^(double progress) {
        } onCompletion:^(NSString *success) {
            [Common showAlertWithTitle:@"Success" message:@"Profile photo uploaded" self:self];
        } onFailure:^(NSString *error) {
            
        }];
        
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}


@end
