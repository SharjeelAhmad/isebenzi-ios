//
//  PersonalInformationViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "PersonalInformationViewController.h"

@interface PersonalInformationViewController ()

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *surName;
@property (weak, nonatomic) IBOutlet UITextField *mobileNum;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *passportNum;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;

@property (weak, nonatomic) IBOutlet CustomScroll *scrollView;

@end

@implementation PersonalInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView drawRect:CGRectZero];
    [self setPlaceHolderTextColor:_firstName.textColor];
    
    _firstName.text = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_F_NAME];
    _surName.text = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_L_NAME];
    _mobileNum.text = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_MOBILE];
    _passportNum.text = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_PASSPORT];
    _password.text = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_PASSWORD];
    _confirmPassword.text = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_PASSWORD];
}

-(void)setPlaceHolderTextColor:(UIColor*)color {
    [_firstName setValue:color
              forKeyPath:@"_placeholderLabel.textColor"];
    [_surName setValue:color
            forKeyPath:@"_placeholderLabel.textColor"];
    [_mobileNum setValue:color
              forKeyPath:@"_placeholderLabel.textColor"];
    [_passportNum setValue:color
                forKeyPath:@"_placeholderLabel.textColor"];
    [_password setValue:color
             forKeyPath:@"_placeholderLabel.textColor"];
    [_confirmPassword setValue:color
                    forKeyPath:@"_placeholderLabel.textColor"];
}

- (IBAction)saveInfo:(UIButton *)sender {
    [Server editProfile:_firstName.text surName:_surName.text mobile:_mobileNum.text passport:_passportNum.text pass:_password.text onSuccess:^(NSString *arr) {
        [Common showAlertWithTitle:@"Success" message:@"Profile updated" self:self];
        [Server login:[[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_EMAIL] andPassword:_password.text andType:[[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_TYPE] onSuccess:^(NSDictionary *dict) {
            
        } onFailure:^(NSString *err) {
            
        }];
    } onFailure:^(NSString *error) {
        [Common showAlertWithTitle:@"Oops" message:error self:self];
    }];
}


@end
