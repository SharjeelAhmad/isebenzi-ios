//
//  LeftNavViewController.h
//  LeoMarchent
//
//  Created by Sharjeel MacBookPro on 22/07/2016.
//  Copyright © 2016 Sharjeel MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface LeftNavViewController : UIViewController

@property (nonatomic, weak) id <NavigationMenuDelegate> leftMenuDelegate;

@end
