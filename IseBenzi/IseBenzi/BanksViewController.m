//
//  BanksViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 25/05/2018.
//  Copyright © 2018 Takhleeq MacBookPro. All rights reserved.
//

#import "BanksViewController.h"

@interface BanksViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *banks;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spimmer;

@end

@implementation BanksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _banks = [[NSArray alloc] init];
    
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tableView reloadData];
    
    _selectedBank = @"";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [Server getBanksOnSuccess:^(NSArray *banks) {
        _banks = banks;
        [_tableView reloadData];
        [_spimmer setHidden:true];
    } onFailure:^(NSString *err) {
        
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _banks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary *bank = _banks[indexPath.row];
    
    [cell.textLabel setText:bank[@"name"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *bank = _banks[indexPath.row];
    _selectedBank = bank[@"name"];
    [self performSegueWithIdentifier:@"exit" sender:nil];
}

@end
