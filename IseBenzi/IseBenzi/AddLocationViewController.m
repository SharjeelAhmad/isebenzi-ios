//
//  AddLocationViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "AddLocationViewController.h"

@interface AddLocationViewController ()
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation AddLocationViewController {
    CLLocationCoordinate2D selectedLocation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.delegate = self;
    
    if(_location) {
        self.name.text = _location[@"name"];
        self.address.text = [NSString stringWithFormat:@"%@ , %@" , _location[@"latitude"] , _location[@"longitude"]];
        
        CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(([NSString stringWithFormat:@"%@" , _location[@"latitude"]]).doubleValue, ([NSString stringWithFormat:@"%@" , _location[@"longitude"]]).doubleValue);
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 800, 800);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
        
        // Add an annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = loc;
        point.title = _location[@"name"];
        point.subtitle = @"";
        
        [self.mapView addAnnotation:point];
        
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer  alloc] initWithTarget:self action:@selector(longpressToGetLocation:)];
        
        lpgr.minimumPressDuration = 0.5;
        lpgr.delaysTouchesBegan = true;
        lpgr.delegate = self;
    
        [self.mapView addGestureRecognizer:lpgr];
    }
}
    
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
    {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    }


         - (void)longpressToGetLocation:(UIGestureRecognizer *)gestureRecognizer {
             
             if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
                 return;
             
             MKMapView* view = (MKMapView*) gestureRecognizer.view;
             //CGPoint loc = [gestureRecognizer locationInView:view];
             //MKNewAnnotationContainerView* subview = (MKNewAnnotationContainerView *) [view hitTest:loc withEvent:nil];
             
             CGPoint touchPoint = [gestureRecognizer locationInView:view];
             selectedLocation = [view convertPoint:touchPoint toCoordinateFromView:view];
             
             MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
             point.coordinate = selectedLocation;
             for (id annotation in view.annotations) {
                 [view removeAnnotation:annotation];
             }
             [view addAnnotation:point];
         }

- (IBAction)save:(UIBarButtonItem *)sender {
    [Server addLocation:self.name.text lat:[NSString stringWithFormat:@"%f" , selectedLocation.latitude] long:[NSString stringWithFormat:@"%f" , selectedLocation.longitude] onSuccess:^(NSString *dict) {
        [Common showAlertWithTitle:@"Success" message:@"Location added" self:self];
        [self.navigationController popViewControllerAnimated:YES];
    } onFailure:^(NSString *error) {
        [Common showAlertWithTitle:@"Oops" message:error self:self];
    }];
}

@end
