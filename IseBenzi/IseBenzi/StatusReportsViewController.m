//
//  StatusReportsViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "StatusReportsViewController.h"
#import "SeekerJobDetailViewController.h"

@interface StatusReportsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *spinnerView;

@end

@implementation StatusReportsViewController {
    NSArray *dataSource;
    NSDictionary *selectedJob;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view bringSubviewToFront:self.spinnerView];
}

-(void)viewWillAppear:(BOOL)animated {
    self.spinnerView.hidden = NO;
    [Server getSeekerJobsOnSuccess:^(NSArray *arr) {
        dataSource  = arr;
        [self.tableView reloadData];
        self.spinnerView.hidden = YES;
    } onFailure:^(NSString *err) {
        self.spinnerView.hidden = YES;
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = dataSource[indexPath.row][@"from_date"];
    
    if([dataSource[indexPath.row][@"status"] isEqualToString:@"3"])
         cell.detailTextLabel.text  = @"Closed";
    
    if([dataSource[indexPath.row][@"status"] isEqualToString:@"2"])
        cell.detailTextLabel.text  = @"Open";
    
    if([dataSource[indexPath.row][@"status"] isEqualToString:@"1"])
        cell.detailTextLabel.text  = @"Pending";
    
    if([dataSource[indexPath.row][@"status"] isEqualToString:@"0"])
        cell.detailTextLabel.text  = @"New";
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedJob = dataSource[indexPath.row];
    
    [self performSegueWithIdentifier:@"jobDetail" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SeekerJobDetailViewController *seeker = segue.destinationViewController;
    
    seeker.job = selectedJob;
}

@end
