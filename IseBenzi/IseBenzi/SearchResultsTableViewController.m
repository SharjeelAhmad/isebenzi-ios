//
//  SearchResultsTableViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 27/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "SearchResultsTableViewController.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface SearchResultsTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *resultsLabel;


@end

@implementation SearchResultsTableViewController {
    NSArray *dataSource;
    NSMutableArray *selected;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    selected = @[].mutableCopy;
}

-(void)viewWillAppear:(BOOL)animated {
    [Server searchJob:^(NSArray *arr) {
        dataSource = arr;
        [self.tableView reloadData];
        self.resultsLabel.text = [NSString stringWithFormat:@"Search results : %d candidates were found" , (int)arr.count];
    } onFailure:^(NSString *error) {
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return dataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UILabel *name = [cell viewWithTag:1];
    UILabel *rateDay = [cell viewWithTag:2];
    UILabel *rateHour = [cell viewWithTag:3];
    HCSStarRatingView *rating = [cell viewWithTag:4];
    
    name.text = [NSString stringWithFormat:@"%@ %@" , dataSource[indexPath.row][@"firstname"]  , dataSource[indexPath.row][@"lastname"]];
    rateDay.text = dataSource[indexPath.row][@"email"];
    rateHour.text = [NSString stringWithFormat:@"Rate/Hour: $%@" , dataSource[indexPath.row][@"Availibility"][@"rate"]];
    
    rating.value = [NSString stringWithFormat:@"%@" , dataSource[indexPath.row][@"AverageRating"]].floatValue;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark) {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        
        [selected removeObject:dataSource[indexPath.row]];
    }
    else {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [selected addObject:dataSource[indexPath.row]];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)request:(UIBarButtonItem *)sender {
    for (NSDictionary *person in selected) {
        [Server addJob:person[@"id"] occupation:_occupation fromDate:_fromDate toDate:_toDate perDay:_perDay lat:[NSString stringWithFormat:@"%f" , self.region.latitude] long:[NSString stringWithFormat:@"%f" , self.region.longitude] perHOur:self.perDay persons:_persons onSuccess:^(NSString *success) {
            [Common showAlertWithTitle:@"Success" message:@"Request Sent" self:self];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } onFailure:^(NSString *err) {
            [Common showAlertWithTitle:@"Oops" message:err self:self];
        }];
        
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
