//
//  AddRatingViewController.m
//  IseBenzi
//
//  Created by Apple on 14/09/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "AddRatingViewController.h"
#import "HCSStarRatingView.h"

@interface AddRatingViewController ()
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *providerName;
@property (weak, nonatomic) IBOutlet UITextField *commenText;

@end

@implementation AddRatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.providerName.text = self.provider;
}

- (IBAction)saveRating:(UIButton *)sender {
    [Server addRating:[NSString stringWithFormat:@"%f" , _ratingView.value] comment:_commenText.text provider:_provideId onSuccess:^(NSString *success) {
       [self.navigationController popToRootViewControllerAnimated:YES];
    } onFailure:^(NSString *error) {
        
    }];
}

@end
