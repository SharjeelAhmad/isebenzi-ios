//
//  HomeViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 15/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface HomeViewController : UIViewController
@property (nonatomic, weak) id <NavigationMenuDelegate> leftMenuDelegate;

@end
