//
//  DomesticViewController2.m
//  IseBenzi
//
//  Created by Apple on 11/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "DomesticViewController2.h"

@interface DomesticViewController2 ()

@end

@implementation DomesticViewController2 {
    NSArray *questions;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    questions = @[@{@"question":@"9. If the power goes out, what do you do next?", @"a":@" Pack a way everything neatly and go home.", @"b":@"Continue with task that do not require electricity.", @"c":@"Phone your employer and ask if you can leave."},
                  @{@"question":@"10. What is the most important principle for you in a working environment?", @"a":@" Trust.", @"b":@"Punctuality.", @"c":@"Remuneration."},
                  @{@"question":@"11. While ironing, you burn your employer's garment, what do you do next?", @"a":@" Put the garment back into washing.", @"b":@"Put the garment in the closet after your are done.", @"c":@"Put the garment aside to tell your employer."},
                  @{@"question":@"12. When you arive at your employer's premises, you notice that the kitchen tablecloth has stain on it. The tablecloth was not put into the washing basket. What do you do next?", @"a":@" Leave the tablecloth till the employer notices the stain.", @"b":@"Remove the tablecloth and put it in next week's washing basket.", @"c":@"Replace the tablecloth with a clean cloth and put the dirty cloth in the washing basket."}];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return questions.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    NSDictionary *tempDict = [questions objectAtIndex:indexPath.row];
    UILabel *question = (UILabel*)[cell viewWithTag:7];
    UILabel *optionA = (UILabel*)[cell viewWithTag:4];
    UILabel *optionB = (UILabel*)[cell viewWithTag:5];
    UILabel *optionC = (UILabel*)[cell viewWithTag:6];
    
    question.text = tempDict[@"question"];
    optionA.text = tempDict[@"a"];
    optionB.text = tempDict[@"b"];
    optionC.text = tempDict[@"c"];
    
    UIImageView* circule1 = (UIImageView*)[cell viewWithTag:1];
    UIImageView* circule2 = (UIImageView*)[cell viewWithTag:2];
    UIImageView* circule3 = (UIImageView*)[cell viewWithTag:3];
    
    UITapGestureRecognizer *singleTap1 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap1 setNumberOfTapsRequired:1];
    
    UITapGestureRecognizer *singleTap2 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap2 setNumberOfTapsRequired:1];
    
    UITapGestureRecognizer *singleTap3 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap3 setNumberOfTapsRequired:1];
    
    [circule1 addGestureRecognizer:singleTap1];
    [circule2 addGestureRecognizer:singleTap2];
    [circule3 addGestureRecognizer:singleTap3];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
    
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    for(int i=1; i<4; i++ ) {
        ((UIImageView*)[((UITableViewCell*)recognizer.view.superview.superview) viewWithTag:i]).image = [UIImage imageNamed:@"circleEmpty@3.png"];
    }
    ((UIImageView*)recognizer.view).image = [UIImage imageNamed:@"circleFilled@3.png"];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
