//
//  MyProfileViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 16/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "MyProfileViewController.h"

@interface MyProfileViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *rating;

@end

@implementation MyProfileViewController {
    NSArray *staticLabels;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [Common setNavigationTitleFont:self.navigationController];
    staticLabels = @[@"Personal Information", @"View Reviews", @"Banking Details", @"Skills Assessment"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
    
    [self performSelectorInBackground:@selector(getImageFromURL:) withObject:[NSString stringWithFormat:@"%@%@" , SERVER_URL , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_IMAGE]]];
}

-(void) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    dispatch_async(dispatch_get_main_queue(), ^{
       self.userImageView.image = result;
    });
}

-(void)viewWillAppear:(BOOL)animated {
    self.userName.text = [NSString stringWithFormat:@"%@ %@" , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_F_NAME] , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_L_NAME]];
    
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height /2;
    self.userImageView.layer.masksToBounds = YES;
    self.userImageView.layer.borderWidth = 0;
    
    self.rating.value = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_AVERAGE_RATINGS].doubleValue;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return staticLabels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = [staticLabels objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"reveiw" sender:self];
    }
    
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"profile" sender:self];
    }
    
    if (indexPath.row == 2) {
        [self performSegueWithIdentifier:@"banking" sender:self];
    }
    
    if (indexPath.row == 3) {
        [self performSegueWithIdentifier:@"assessment" sender:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (IBAction)uploadProfilePic:(UIButton *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction* openPhotoLibary = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectPhoto:YES];
    }];
    
    UIAlertAction* openCamera = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectPhoto:NO];
    }];
    
    //[openCamera setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    //[openPhotoLibary setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    
    [alertController addAction:openPhotoLibary];
    [alertController addAction:openCamera];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)selectPhoto:(BOOL)camera {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if(camera) picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    else picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        //Or you can get the image url from AssetsLibrary
        //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
        
        self.userImageView.image = image;
        
        [Server uploadPhoto:UIImageJPEGRepresentation(image, 0.6) onProgressUpdate:^(double progress) {
            
        } onCompletion:^(NSString *success) {
            [Common showAlertWithTitle:@"Success" message:@"Profile photo uploaded" self:self];
            [self.leftMenuDelegate refreshLeftScreen];
        } onFailure:^(NSString *error) {
            
        }];
        
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

@end
