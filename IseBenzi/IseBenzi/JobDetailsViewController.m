//
//  JobDetailsViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "JobDetailsViewController.h"

@interface JobDetailsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *complete;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *accept;
@property (weak, nonatomic) IBOutlet UINavigationItem *bar;

@end

@implementation JobDetailsViewController {
    NSArray *labels;
    NSArray *keys;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    labels = @[@"Skills required : ", @"Service Seeker name : ", @"Date : " , @"Time : ", @"Address" , @"Rating"];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if([_job[@"status"] isEqualToString:@"0"]) {
        NSMutableArray *buttons =self.bar.rightBarButtonItems.mutableCopy;
        
        [buttons removeObject:_cancel];
        [buttons removeObject:_complete];
        
        self.bar.rightBarButtonItems = buttons;
    }else if([_job[@"status"] isEqualToString:@"2"]) {
        NSMutableArray *buttons =self.bar.rightBarButtonItems.mutableCopy;
        
        [buttons removeObject:_accept];
        
        self.bar.rightBarButtonItems = buttons;
    }else {
        NSMutableArray *buttons =self.bar.rightBarButtonItems.mutableCopy;
        
        [buttons removeAllObjects];
        
        self.bar.rightBarButtonItems = buttons;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return labels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = labels[indexPath.row];
    
    if(indexPath.row == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@" ,cell.textLabel.text , self.job[@"occupation"]];
    } else if(indexPath.row == 1) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@" , cell.textLabel.text , self.job[@"seeker"]];
    }else if(indexPath.row == 2) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@" ,  cell.textLabel.text , [self.job[@"from_date"] componentsSeparatedByString:@" "].firstObject];
    }else if(indexPath.row == 3) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@"   , cell.textLabel.text , [self.job[@"from_date"] componentsSeparatedByString:@" "].lastObject];
    }
    
    return cell;
}

- (IBAction)acceptJob:(UIBarButtonItem *)sender {
    [Server changeJobStatus:@"1" job:self.job[@"id"] onSuccess:^(NSArray *dict) {
        [self.navigationController popViewControllerAnimated:YES];
    } onFailure:^(NSString *error) {
        [Common showAlertWithTitle:@"Oops" message:error self:self];
    }];
}

- (IBAction)closeJob:(UIBarButtonItem *)sender {
    [Server changeJobStatus:@"3" job:self.job[@"id"] onSuccess:^(NSArray *dict) {
        [self.navigationController popViewControllerAnimated:YES];
    } onFailure:^(NSString *error) {
        [Common showAlertWithTitle:@"Oops" message:error self:self];
    }];
}

@end
