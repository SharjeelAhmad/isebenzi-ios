//
//  SeekerJobDetailViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"

@interface SeekerJobDetailViewController : UIViewController
<UITableViewDelegate , UITableViewDataSource , PayPalPaymentDelegate, PayPalFuturePaymentDelegate, PayPalProfileSharingDelegate>

@property (strong, nonatomic) NSDictionary *job;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *resultText;

@end
