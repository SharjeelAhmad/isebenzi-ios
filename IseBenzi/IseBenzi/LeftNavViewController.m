//
//  LeftNavViewController.m
//  LeoMarchent
//
//  Created by Sharjeel MacBookPro on 22/07/2016.
//  Copyright © 2016 Sharjeel MacBookPro. All rights reserved.
//

#import "LeftNavViewController.h"

@interface LeftNavViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end

@implementation LeftNavViewController {
    NSArray *headings;
    NSMutableArray *translatedHeadings;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _userImageView.layer.cornerRadius = 65;
    _userImageView.layer.masksToBounds = YES;
    
    [self reloadData];
}

-(void) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.userImageView.image = result;
    });
}

-(void)viewWillAppear:(BOOL)animated {
    self.userName.text = [NSString stringWithFormat:@"%@ %@" , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_F_NAME] , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_L_NAME]];
    
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height /2;
    self.userImageView.layer.masksToBounds = YES;
    self.userImageView.layer.borderWidth = 0;
    
    [self performSelectorInBackground:@selector(getImageFromURL:) withObject:[NSString stringWithFormat:@"%@%@" , SERVER_URL , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_IMAGE]]];
}

-(void)viewWillDisappear:(BOOL)animated {
}

-(void) reloadData {
    headings = @[@"Home",@"Profile",@"My JOBS", @"Logout"];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return headings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:identifier];
    }
    cell.textLabel.text = [headings objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *heading = headings[indexPath.row];
    
    if ([heading isEqualToString:@"Home"])
        [self.leftMenuDelegate toggleHomeScreen];
    
    if ([heading isEqualToString:@"Profile"])
        //[self.leftMenuDelegate toggleLoginScreen];
        [self.leftMenuDelegate toggleProfileScreen];
    
    if ([heading isEqualToString:@"My JOBS"])
       // [self logOut];
        [self.leftMenuDelegate toggleJobsScreen];
    
    if ([heading isEqualToString:@"HELP"])
       // [self.leftMenuDelegate toggleSearchScreen];
    
    if ([heading isEqualToString:@"HELP AND SETTINGS"])
       // [self.leftMenuDelegate toggleAboutScreen];
    
    if ([heading isEqualToString:@"SHARE APP"])
        NSLog(@"");
       // [self.leftMenuDelegate toggleInsertScreen];
    
    if ([heading isEqualToString:@"Logout"])
        [self.leftMenuDelegate logout];
    
}

@end
