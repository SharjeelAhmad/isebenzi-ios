//
//  GardenerViewController.m
//  IseBenzi
//
//  Created by Apple on 11/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "GardenerViewController.h"

@interface GardenerViewController ()

@end

@implementation GardenerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [NSString stringWithFormat:@"cell%ld",(long)indexPath.row];
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:identifier];
    
    UIImageView* circule1 = (UIImageView*)[cell viewWithTag:1];
    UIImageView* circule2 = (UIImageView*)[cell viewWithTag:2];
    UIImageView* circule3 = (UIImageView*)[cell viewWithTag:3];
    
    UITapGestureRecognizer *singleTap1 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap1 setNumberOfTapsRequired:1];
    
    UITapGestureRecognizer *singleTap2 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap2 setNumberOfTapsRequired:1];
    
    UITapGestureRecognizer *singleTap3 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap3 setNumberOfTapsRequired:1];
    
    [circule1 addGestureRecognizer:singleTap1];
    [circule2 addGestureRecognizer:singleTap2];
    [circule3 addGestureRecognizer:singleTap3];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 11) {
        return 100;
    }
    else if(indexPath.row == 10) {
        return 150;
    }
    else if(indexPath.row == 9) {
        return 130;
    }
    else if(indexPath.row == 5) {
        return 130;
    }
    return 207;
    //return UITableViewAutomaticDimension;
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    for(int i=1; i<4; i++ ) {
        ((UIImageView*)[((UITableViewCell*)recognizer.view.superview.superview) viewWithTag:i]).image = [UIImage imageNamed:@"circleEmpty@3.png"];
    }
    ((UIImageView*)recognizer.view).image = [UIImage imageNamed:@"circleFilled@3.png"];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
