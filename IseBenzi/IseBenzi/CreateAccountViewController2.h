//
//  CreateAccountViewController2.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 07/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateAccountViewController2 : UIViewController <UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *surName;
@property (strong, nonatomic) NSString *mobileNum;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *passportNum;
@property (strong, nonatomic) UIImage *passImageView;

@end
