//
//  WaiterViewController.m
//  IseBenzi
//
//  Created by Apple on 11/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "WaiterViewController.h"

@interface WaiterViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation WaiterViewController{
    NSArray *questions;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    questions = @[@{@"question":@"1. What does 'a la carte' mean?", @"a":@" Ordering meat that is medium to rear.", @"b":@"Ordering items from a menu.", @"c":@" Ordering white wine."},
                  @{@"question":@"2. What is a Chenin Blanc?", @"a":@" Mexican food.", @"b":@" White wine.", @"c":@"Sharp steak knife."},
                  @{@"question":@"3. What is NOT a vegetarian meal?", @"a":@" pinach Enchiladas", @"b":@"Stuffed mushrooms", @"c":@"Spiced meatballs"},
                  @{@"question":@"4. How many shifts did you work in the last 30 days?", @"a":@" 0 - 8", @"b":@"9 - 17", @"c":@"18 +"},
                  @{@"question":@"5. What is most important principle for you in a working environment?", @"a":@"Punctuality", @"b":@"Trust", @"c":@"Remuneration"},
                  @{@"question":@"5. What is most important principle for you in a working environment?", @"a":@"Punctuality", @"b":@"Trust", @"c":@"Remuneration"},
                  @{@"question":@"6. On the way to work, your car breaks down. What do you do next?", @"a":@" Let you employer know that you will be late.", @"b":@"Assess your available time, call a friend or a taxi.", @"c":@"Fix your car and get to work as soon as possible."},
                  @{@"question":@"7. If you get a 25c tip, what do you do next?", @"a":@"Continue serving the client with a smile.", @"b":@"Demand a bigger tip.", @"c":@"Show your disgust with the appropriate body language only."},
                  @{@"question":@"8. If a customer ask you to make a recommendation for a meal, what will you do?", @"a":@"Recommend a meal that is your favourite.", @"b":@"Recommend the most expensive meal on the menu.", @"c":@"Recommend the meal that most customer order and gets positive feedback."},
                  @{@"question":@"9. If a client orders something that is not on the menu, what will you do next?", @"a":@"Indicate to the client that specific items is not on the menu but you can suggest a similar meal.", @"b":@"Just indicate that the meal is not on the menu and he/she should order something else.", @"c":@" Call the manager to explain to the client that the meal is not on the menu."},
                  @{@"question":@"10. What do you think comes first?", @"a":@"An employee that is remunerated well.", @"b":@"An employee that performs above expectations.", @"c":@"An employee that performs above expectations."}];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return questions.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if(!cell) //Every time cell is nil, dequeue not working
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
        
        
    NSDictionary *tempDict = [questions objectAtIndex:indexPath.row];
    UILabel *question = (UILabel*)[cell viewWithTag:7];
    UILabel *optionA = (UILabel*)[cell viewWithTag:4];
    UILabel *optionB = (UILabel*)[cell viewWithTag:5];
    UILabel *optionC = (UILabel*)[cell viewWithTag:6];
    
    question.text = tempDict[@"question"];
    optionA.text = tempDict[@"a"];
    optionB.text = tempDict[@"b"];
    optionC.text = tempDict[@"c"];
    
    UIImageView* circule1 = (UIImageView*)[cell viewWithTag:1];
    UIImageView* circule2 = (UIImageView*)[cell viewWithTag:2];
    UIImageView* circule3 = (UIImageView*)[cell viewWithTag:3];
    
    UITapGestureRecognizer *singleTap1 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap1 setNumberOfTapsRequired:1];
    
    UITapGestureRecognizer *singleTap2 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap2 setNumberOfTapsRequired:1];
    
    UITapGestureRecognizer *singleTap3 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap3 setNumberOfTapsRequired:1];
    
    [circule1 addGestureRecognizer:singleTap1];
    [circule2 addGestureRecognizer:singleTap2];
    [circule3 addGestureRecognizer:singleTap3];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
    
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    for(int i=1; i<4; i++ ) {
        ((UIImageView*)[((UITableViewCell*)recognizer.view.superview.superview) viewWithTag:i]).image = [UIImage imageNamed:@"circleEmpty@3.png"];
    }
    ((UIImageView*)recognizer.view).image = [UIImage imageNamed:@"circleFilled@3.png"];
    [_tableView reloadData];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
