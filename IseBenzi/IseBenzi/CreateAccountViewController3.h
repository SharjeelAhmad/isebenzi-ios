//
//  CreateAccountViewController3.h
//  IseBenzi
//
//  Created by Apple on 10/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface CreateAccountViewController3 : UIViewController <MKMapViewDelegate , UIGestureRecognizerDelegate>

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *surName;
@property (strong, nonatomic) NSString *mobileNum;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *passportNum;

@property (strong, nonatomic) NSString *bankName;
@property (strong, nonatomic) NSString *bankCode;
@property (strong, nonatomic) NSString *accountHolderName;
@property (strong, nonatomic) NSString *accountNumber;

@property (strong, nonatomic) UIImage *userImageView;
@property (strong, nonatomic) UIImage *passImageView;

@end
