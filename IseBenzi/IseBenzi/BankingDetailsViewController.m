//
//  BankingDetailsViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "BankingDetailsViewController.h"
#import "BanksTableViewController.h"

@interface BankingDetailsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpace;


@end

@implementation BankingDetailsViewController {
    NSArray *labels;
    NSString *bankName;
    NSString *bankCode;
    NSString *accountHolderName;
    NSString *accountNumber;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    labels = @[@"Bank name", @"Bank Code", @"Account Holder Name" , @"Account Number"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    bankName = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_BANK_NAME];
    bankCode = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_BANK_CODE];
    accountNumber = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_BANK_ANUMBER];
    accountHolderName = [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_BANK_UNAME];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return labels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UITextField *text = [cell viewWithTag:1];
    
    switch (indexPath.row) {
        case 0:
            text.text = bankName;
            break;
        case 1:
            text.text = bankCode;
            break;
        case 2:
            text.text = accountHolderName;
            break;
        case 3:
            text.text = accountNumber;
            break;
            
        default:
            break;
    }
    
    text.placeholder = [labels objectAtIndex:indexPath.row];
    [text setValue:[UIColor redColor]
        forKeyPath:@"_placeholderLabel.textColor"];
    
    return cell;
}

- (IBAction)textChange:(UITextField *)sender {
    if([sender.placeholder isEqualToString:labels[0]]) {
        bankName = sender.text;
    }else if([sender.placeholder isEqualToString:labels[1]]) {
        bankCode = sender.text;
    }else if([sender.placeholder isEqualToString:labels[2]]) {
        accountHolderName = sender.text;
    }else if([sender.placeholder isEqualToString:labels[3]]) {
        accountNumber = sender.text;
    }
}

- (IBAction)editBegin:(UITextField *)sender {
    if([sender.placeholder isEqualToString:labels[0]]) {
        [self performSegueWithIdentifier:@"banks" sender:self];
        [sender resignFirstResponder];
    }
}

- (void)keyboardDidShow: (NSNotification *) notif{
    NSDictionary *info  = notif.userInfo;
    NSValue *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    self.bottomSpace.constant = 10 + keyboardFrame.size.height;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    self.bottomSpace.constant = 32;
}

- (IBAction)save:(UIButton *)sender {
    [Server editBank:accountNumber branchCode:bankCode bankName:bankName accName:accountHolderName onSuccess:^(NSString *ss) {
        [Common showAlertWithTitle:@"Success" message:@"Bank details updated" self:self];
        
        [Server login:[[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_EMAIL] andPassword:[[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_PASSWORD] andType:[[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_TYPE] onSuccess:^(NSDictionary *dict) {
            
        } onFailure:^(NSString *err) {
            
        }];
    } onFailure:^(NSString *err) {
       [Common showAlertWithTitle:@"Oops" message:err self:self];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

}

- (IBAction)unwindForSegue:(UIStoryboardSegue *)unwindSegue towardsViewController:(UIViewController *)subsequentVC {
    BanksTableViewController *banks = unwindSegue.sourceViewController;
    bankName = banks.selectedBank;
    [_tableView reloadData];
}

@end
