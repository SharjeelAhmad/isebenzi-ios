//
//  BanksViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 25/05/2018.
//  Copyright © 2018 Takhleeq MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BanksViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSString *selectedBank;
@end
