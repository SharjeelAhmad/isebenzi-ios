//
//  MyJobsViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 18/06/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface MyJobsViewController : UIViewController <UITableViewDataSource , UITableViewDelegate>
@property (nonatomic, weak) id <NavigationMenuDelegate> leftMenuDelegate;

@end
