//
//  Common.h
//  IseBenzi
//
//  Created by Sharjeel MacBookPro on 24/05/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Common : NSData

+(void)setNavigationTitleFont: (UINavigationController*)navi;
+ (void) showAlertWithTitle:(NSString *)title message:(NSString *)message self:(UIViewController *)controller;
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;

@end
