//
//  DomesticViewController.h
//  IseBenzi
//
//  Created by Apple on 11/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DomesticViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@end
