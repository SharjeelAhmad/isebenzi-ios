//
//  MyProfileViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 16/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface MyProfileViewController : UIViewController<UITableViewDelegate, UITableViewDataSource , UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, weak) id <NavigationMenuDelegate> leftMenuDelegate;
@end
