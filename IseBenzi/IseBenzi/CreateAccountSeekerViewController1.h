//
//  CreateAccountSeekerViewController1.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 07/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface CreateAccountSeekerViewController1 : UIViewController <MKMapViewDelegate , UIGestureRecognizerDelegate , UITextFieldDelegate>

@end
