//
//  Common.m
//  IseBenzi
//
//  Created by Sharjeel MacBookPro on 24/05/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "Common.h"

@implementation Common

+(void)setNavigationTitleFont: (UINavigationController*)navi{
    [navi.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,
      NAVIGATION_FONT, NSFontAttributeName,nil]];
    
    navi.navigationBar.tintColor = [UIColor whiteColor];
}

+ (void) showAlertWithTitle:(NSString *)title message:(NSString *)message self:(UIViewController *)controller {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIAlertController class]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title" message:@"text mssg" preferredStyle:UIAlertControllerStyleAlert];
            alert.title = title;
            alert.message = message;
            
            UIAlertAction* cancel =  [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            [alert addAction:cancel];
            
            [controller presentViewController:alert animated:YES completion:^{
            }];
        } else {
        }
    });
}

+(BOOL) NSStringIsValidEmail:(NSString *)checkString {
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
