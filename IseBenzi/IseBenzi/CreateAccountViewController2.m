//
//  CreateAccountViewController2.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 07/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "CreateAccountViewController2.h"
#import "CreateAccountViewController3.h"
#import "BanksViewController.h"

@interface CreateAccountViewController2 ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpace;

@end

@implementation CreateAccountViewController2 {
    NSArray *labels;
    NSString *bankName;
    NSString *bankCode;
    NSString *accountHolderName;
    NSString *accountNumber;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    labels = @[@"Account holder name*", @"Account Number*", @"Bank Name*" , @"Branch Code*"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    bankName = @"";
    bankCode = @"";
    accountNumber = @"";
    accountHolderName = @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return labels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UITextField *text = [cell viewWithTag:1];
    text.placeholder = [labels objectAtIndex:indexPath.row];
    [text setValue:[UIColor redColor]
        forKeyPath:@"_placeholderLabel.textColor"];
    
    if(indexPath.row == 2) {
        text.text = bankName;
    }else if(indexPath.row == 0) {
        text.text = accountHolderName;
    }

    return cell;
}

- (IBAction)uploadPictuer:(UIButton *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction* openPhotoLibary = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectPhoto:YES];
    }];
    
    UIAlertAction* openCamera = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectPhoto:NO];
    }];
    
    [alertController addAction:openPhotoLibary];
    [alertController addAction:openCamera];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)selectPhoto:(BOOL)camera {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if(camera) picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    else picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        //Or you can get the image url from AssetsLibrary
        //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
        self.userImageView.image = image;
    }];
}

- (void)keyboardDidShow: (NSNotification *) notif{
    NSDictionary *info  = notif.userInfo;
    NSValue *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    self.bottomSpace.constant = 10 + keyboardFrame.size.height;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    self.bottomSpace.constant = 32;
}

- (IBAction)begin:(UITextField *)sender {
    if([sender.placeholder isEqualToString:labels[2]]) {
        [self performSegueWithIdentifier:@"banks" sender:self];
    }
}


- (IBAction)textChange:(UITextField *)sender {
    if([sender.placeholder isEqualToString:labels[0]]) {
        accountHolderName = sender.text;
    }else if([sender.placeholder isEqualToString:labels[1]]) {
        accountNumber = sender.text;
    }else if([sender.placeholder isEqualToString:labels[2]]) {
        bankName = sender.text;
    }else if([sender.placeholder isEqualToString:labels[3]]) {
        bankCode = sender.text;
    }
}

- (IBAction)next:(UIButton *)sender {
    if([bankName isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Bank name required" self:self];
    else if([bankCode isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Branch code required" self:self];
    else if([accountNumber isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Account number required" self:self];
    else if([accountHolderName isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Account title required" self:self];
    
    else [self performSegueWithIdentifier:@"goToNext" sender:self];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if(segue.destinationViewController.class == CreateAccountViewController3.class) {
        CreateAccountViewController3 *acc = segue.destinationViewController;
        acc.firstName = self.firstName;
        acc.surName = self.surName;
        acc.mobileNum = self.mobileNum;
        acc.email = self.email;
        acc.password = self.password;
        acc.passportNum = self.passportNum;
        
        acc.bankName = bankName;
        acc.bankCode = bankCode;
        acc.accountNumber = accountNumber;
        acc.accountHolderName = accountHolderName;
        
        acc.userImageView = self.userImageView.image;
        acc.passImageView = self.passImageView;
    }
}

- (void)unwindForSegue:(UIStoryboardSegue *)unwindSegue towardsViewController:(UIViewController *)subsequentVC {
    BanksViewController *bank = unwindSegue.sourceViewController;
    bankName = bank.selectedBank;
    [_tableView reloadData];
}

@end
