//
//  CreateAccountSeekerViewController1.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 07/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "CreateAccountSeekerViewController1.h"
#import <MapKit/MapKit.h>

@interface CreateAccountSeekerViewController1 ()
@property (weak, nonatomic) IBOutlet UITextField *fName;
@property (weak, nonatomic) IBOutlet UITextField *surName;
@property (weak, nonatomic) IBOutlet UITextField *passportNumber;
@property (weak, nonatomic) IBOutlet UITextField *cellNumber;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UISwitch *businessSwitch;
@property (weak, nonatomic) IBOutlet UIView *spinnerView;
@property (weak, nonatomic) IBOutlet CustomScroll *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *cPassword;

@end

@implementation CreateAccountSeekerViewController1 {
    CLLocationCoordinate2D selectedLocation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UINavigationBar appearance] setBarTintColor:NAVIGATION_BACKGROUND_SEEKER_COLOR];
    [Common setNavigationTitleFont:self.navigationController];
    [self setPlaceHolderTextColor:NAVIGATION_BACKGROUND_SEEKER_COLOR];
    [self.scrollView drawRect:CGRectZero];
    
    [self.view bringSubviewToFront:self.spinnerView];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer  alloc] initWithTarget:self action:@selector(longpressToGetLocation:)];
    
    lpgr.minimumPressDuration = 0.5;
    lpgr.delaysTouchesBegan = true;
    lpgr.delegate = self;
    
    [self.mapView addGestureRecognizer:lpgr];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setPlaceHolderTextColor:(UIColor*)color {
    [_fName setValue:color
              forKeyPath:@"_placeholderLabel.textColor"];
    [_surName setValue:color
            forKeyPath:@"_placeholderLabel.textColor"];
    [_passportNumber setValue:color
              forKeyPath:@"_placeholderLabel.textColor"];
    [_cellNumber setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)longpressToGetLocation:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    MKMapView* view = (MKMapView*) gestureRecognizer.view;
    //CGPoint loc = [gestureRecognizer locationInView:view];
    //MKNewAnnotationContainerView* subview = (MKNewAnnotationContainerView *) [view hitTest:loc withEvent:nil];
    
    CGPoint touchPoint = [gestureRecognizer locationInView:view];
    selectedLocation = [view convertPoint:touchPoint toCoordinateFromView:view];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = selectedLocation;
    for (id annotation in view.annotations) {
        [view removeAnnotation:annotation];
    }
    [view addAnnotation:point];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)next:(UIButton *)sender {
    if([_fName.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Firstname required" self:self];
    else if([_surName.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Surname required" self:self];
    else if([_email.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Email required" self:self];
    else if([_password.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Password required" self:self];
    else if([_cPassword.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Conform Password required" self:self];
    else if(![_cPassword.text isEqualToString:_password.text]) [Common showAlertWithTitle:@"Oops" message:@"Passwords don't match" self:self];
    else if([_passportNumber.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Passport required" self:self];
    else if([_cellNumber.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Cell required" self:self];
    else if(selectedLocation.latitude == 0) [Common showAlertWithTitle:@"Oops" message:@"Map Location required" self:self];
    
    else {
        self.spinnerView.hidden = NO;
        
        [Server signupSeeker:_email.text andPassword:_password.text firstname:_fName.text lastname:_surName.text phone:_cellNumber.text passport:_passportNumber.text image:UIImageJPEGRepresentation([UIImage imageNamed:@"userPic"], 0.6)  lat:[NSString stringWithFormat:@"%f", selectedLocation.latitude] long:[NSString stringWithFormat:@"%f", selectedLocation.longitude] address:@"" locName:@"Default" onProgressUpdate:^(double progress) {
            
        } onSuccess:^(NSDictionary *suc) {
            [Server login:_email.text andPassword:_password.text andType:@"1" onSuccess:^(NSDictionary *dict) {
                [self performSegueWithIdentifier:@"goToDashboard" sender:self];
                self.spinnerView.hidden = YES;
            } onFailure:^(NSString *error) {
                [Common showAlertWithTitle:@"Oops" message:@"Login Failed" self:self];
                self.spinnerView.hidden = YES;
            }];
        } onFailure:^(NSString *err) {
            self.spinnerView.hidden = YES;
            [Common showAlertWithTitle:@"Oops" message:err self:self];
        }];
    }
    
}


@end
