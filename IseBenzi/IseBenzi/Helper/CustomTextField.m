//
//  CustomTextField.m
//  IseBenzi
//
//  Created by Sharjeel MacBookPro on 24/05/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

//- (CGRect)textRectForBounds:(CGRect)bounds {
//    return CGRectInset(bounds, 40, 0);
//}
//
//- (CGRect)editingRectForBounds:(CGRect)bounds {
//    return CGRectInset(bounds, 40, 0);
//}
- (void) drawPlaceholderInRect:(CGRect)rect {
    
    [[UIColor blueColor] setFill];
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [super drawPlaceholderInRect:rect];
}

@end
