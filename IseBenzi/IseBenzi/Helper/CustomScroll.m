//
//  CustomScroll.m
//  Danat
//
//  Created by Sharjeel iMac on 1/25/17.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "CustomScroll.h"

@implementation CustomScroll

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    //[self setBackgroundColor:[UIColor whiteColor]];
    [self setShowsHorizontalScrollIndicator:NO];
    [self setShowsVerticalScrollIndicator:NO];
    
//    CGRect contentRect = CGRectZero;
//    for (UIView *view in self.superview.subviews) {
//        contentRect = CGRectUnion(contentRect, view.frame);
//    }
//    //self.contentSize = contentRect.size;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.superview.window];

}

- (void)keyboardWillHide:(NSNotification *)n {
    NSLayoutConstraint *bottom = [self constraintForIdentifier:@"bottom" andview:self.superview];
    bottom.constant = 0;
}

- (void)keyboardWillShow:(NSNotification *)n {
    NSDictionary* userInfo = [n userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    NSLayoutConstraint *bottom = [self constraintForIdentifier:@"bottom" andview:self.superview];
    bottom.constant = keyboardSize.height;
}

-(NSLayoutConstraint *)constraintForIdentifier:(NSString *)identifier andview:(UIView *)view {
    for (NSLayoutConstraint *constraint in view.constraints) {
        if ([constraint.identifier isEqualToString:identifier]) {
            return constraint;
        }
    }
    return nil;
}

@end
