//
//  HomeViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 15/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "HomeViewController.h"
#import "JobDetailsViewController.h"

@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *dataSource;
@property (weak, nonatomic) IBOutlet UILabel *nooffers;

@end

@implementation HomeViewController {
    NSDictionary *selectedJob;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [Common setNavigationTitleFont:self.navigationController];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];    
}

-(void)viewWillAppear:(BOOL)animated {
    [Server getProviderJobsOnSuccess:^(NSArray *array) {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"status contains[cd] %@", @"0"];
        self.dataSource = [array filteredArrayUsingPredicate:bPredicate];

        
        [self.tableView reloadData];
        if(_dataSource.count == 0) _nooffers.hidden = NO;
    } onFailure:^(NSString *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *name = [cell viewWithTag:1];
    UILabel *date = [cell viewWithTag:2];
    
    name.text = self.dataSource[indexPath.row][@"seeker"];
    date.text = self.dataSource[indexPath.row][@"from_date"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedJob = self.dataSource[indexPath.row];
    JobDetailsViewController *jobDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"jobDetails"];
    jobDetails.job = selectedJob;
    
    [self.navigationController pushViewController:jobDetails animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    JobDetailsViewController *jobDetails = segue.destinationViewController;
    jobDetails.job = selectedJob;
}


@end
