//
//  AddLocationViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface AddLocationViewController : UIViewController <MKMapViewDelegate , UIGestureRecognizerDelegate>

@property (strong , nonatomic) NSDictionary *location;

@end
