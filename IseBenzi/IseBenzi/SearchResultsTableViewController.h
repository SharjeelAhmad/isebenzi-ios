//
//  SearchResultsTableViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 27/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SearchResultsTableViewController : UITableViewController

@property (strong , nonatomic) NSString *occupation;
@property (strong , nonatomic) NSString *persons;
@property (strong , nonatomic) NSString *fromDate;
@property (strong , nonatomic) NSString *toDate;
@property (strong , nonatomic) NSString *perDay;
@property (nonatomic) CLLocationCoordinate2D region;

@end
