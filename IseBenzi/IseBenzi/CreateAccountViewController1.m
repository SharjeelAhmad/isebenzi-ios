//
//  CreateAccountViewController1.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 06/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "CreateAccountViewController1.h"
#import "CreateAccountViewController2.h"

@interface CreateAccountViewController1 ()

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *surName;
@property (weak, nonatomic) IBOutlet UITextField *mobileNum;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *passportNum;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;
@property (weak, nonatomic) IBOutlet UISwitch *terms;
@property (weak, nonatomic) IBOutlet CustomScroll *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *passImageView;

@end

@implementation CreateAccountViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView drawRect:CGRectZero];
    [self setPlaceHolderTextColor:[UIColor redColor]];
    [_terms setOn:NO];
}

-(void)setPlaceHolderTextColor:(UIColor*)color {
    [_firstName setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
    [_surName setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
    [_mobileNum setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
    [_email setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
    [_passportNum setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
    [_password setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
    [_confirmPassword setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
}

- (IBAction)uploadPictuer:(UIButton *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction* openPhotoLibary = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectPhoto:YES];
    }];
    
    UIAlertAction* openCamera = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectPhoto:NO];
    }];
    
    [alertController addAction:openPhotoLibary];
    [alertController addAction:openCamera];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)selectPhoto:(BOOL)camera {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if(camera) picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    else picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        //Or you can get the image url from AssetsLibrary
        //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
        self.passImageView.image = image;
    }];
}

- (IBAction)goToNext:(UIButton *)sender {
    if([_firstName.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"First name required" self:self];
    else if([_surName.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Last name required" self:self];
    else if([_mobileNum.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Mobile number required" self:self];
    else if([_email.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Email required" self:self];
    else if([_passportNum.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Passport number required" self:self];
    else if([_password.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Password required" self:self];
    else if([_confirmPassword.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Confirm password required" self:self];
    else if(![_confirmPassword.text isEqualToString:_password.text]) [Common showAlertWithTitle:@"Oops" message:@"Passwords don't match" self:self];
    else if(!self.passImageView.image) [Common showAlertWithTitle:@"Oops" message:@"Passport image required" self:self];
    else if(!_terms.isOn) [Common showAlertWithTitle:@"Oops" message:@"Accept terms and conditions first" self:self];
    else if(![Common NSStringIsValidEmail:_email.text]) [Common showAlertWithTitle:@"Oops" message:@"Email not valid" self:self];
    
    else [self performSegueWithIdentifier:@"goToNext" sender:self];
}
    
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CreateAccountViewController2 *acc = segue.destinationViewController;
    acc.firstName = self.firstName.text;
    acc.surName = self.surName.text;
    acc.mobileNum = self.mobileNum.text;
    acc.email = self.email.text;
    acc.password = self.password.text;
    acc.passportNum = self.passportNum.text;
    acc.passImageView = self.passImageView.image;
}

@end
