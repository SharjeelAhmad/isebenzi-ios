//
//  Server.h
//  EAPrototype
//
//  Created by Sharjeel iMac on 11/1/13.
//  Copyright (c) 2013 Sharjeel iMac. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Server : NSObject

+ (void) login:(NSString *)email andPassword:(NSString *)password andType:(NSString *)type onSuccess:(void(^)(NSDictionary *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (NSURLSessionUploadTask *) signupProvider:(NSString *)email andPassword:(NSString *)password firstname:(NSString*)fName lastname:(NSString*)lName phone:(NSString*)phone passport:(NSString*)passport image:(NSData *)image accountNumber:(NSString *)accountNumber branch:(NSString *)branchCode bankName:(NSString *)bankName accountName:(NSString *)accountName lat:(NSString *)latitude long:(NSString *)longitude address:(NSString *)address rate:(NSString *)rate radius:(NSString *)radius toDate:(NSString *)toDate fromDate:(NSString *)fromDate passImage:(NSData *)passImage onProgressUpdate:(void(^)(double progress))pUpdate onSuccess:(void(^)(NSDictionary *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (NSURLSessionUploadTask *) signupSeeker:(NSString *)email andPassword:(NSString *)password firstname:(NSString*)fName lastname:(NSString*)lName phone:(NSString*)phone passport:(NSString*)passport image:(NSData *)image lat:(NSString *)latitude long:(NSString *)longitude address:(NSString *)address locName:(NSString *)locName onProgressUpdate:(void(^)(double progress))pUpdate onSuccess:(void(^)(NSDictionary *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) getProviderJobsOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) getRatingsOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) getBanksOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (NSURLSessionUploadTask *) uploadPhoto:(NSData *)image onProgressUpdate:(void(^)(double progress))pUpdate onCompletion:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) changeJobStatus:(NSString *)status job:(NSString *)jobId onSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) editProfile:(NSString *)firstName surName:(NSString *)surName mobile:(NSString *)mobile passport:(NSString *)passport pass:(NSString *)pass onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) editBank:(NSString *)accountNumber branchCode:(NSString *)branchCode bankName:(NSString *)bankName accName:(NSString *)accName onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) getSeekerJobsOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) getLocationsOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) addLocation:(NSString *)name lat:(NSString *)lat long:(NSString *)longi onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) searchJob:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) addJob:(NSString *)provider occupation:(NSString *)occupation fromDate:(NSString *)fromDate toDate:(NSString *)toDate perDay:(NSString *)perDay lat:(NSString *)lat long:(NSString *)longi perHOur:(NSString *)perHOur persons:(NSString *)persons onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) requestPayment:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

+ (void) addRating:(NSString *)rating comment:(NSString *)comment provider:(NSString *)provider onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion;

@end
