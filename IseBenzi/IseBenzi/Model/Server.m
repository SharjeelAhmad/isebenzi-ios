//
//  Server.m
//  EAPrototype
//
//  Created by Sharjeel iMac on 11/1/13.
//  Copyright (c) 2013 Sharjeel iMac. All rights reserved.
//

#import "Server.h"
#import "AFNetworking.h"

@implementation Server {
    
}

+ (void) addJob:(NSString *)provider occupation:(NSString *)occupation fromDate:(NSString *)fromDate toDate:(NSString *)toDate perDay:(NSString *)perDay lat:(NSString *)lat long:(NSString *)longi perHOur:(NSString *)perHOur persons:(NSString *)persons onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"seeker" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID] ,
                                      @"provider" : provider,
                                      @"status" : @"0",
                                      @"occupation" : occupation,
                                      @"from_date" : fromDate,
                                      @"to_date" : toDate,
                                      @"per_day" : perDay,
                                      @"per_hour" : perHOur,
                                      @"latitude" : lat,
                                      @"longitude" : longi,
                                      @"persons" : persons
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , ADD_JOB] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else if([responseObject[@"message"] isEqualToString:@"success"]){
                NSDictionary *dict = responseObject;
                sCompletion(dict[@"message"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}


+ (void) requestPayment:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"id" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID]
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , REQUEST_PAYMENT] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else if([responseObject[@"message"] isEqualToString:@"success"]){
                NSDictionary *dict = responseObject;
                sCompletion(dict[@"message"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void) searchJob:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"user_id" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID]
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , JOB_SEARCH] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else {
                NSDictionary *dict = responseObject;
                NSLog(@"providers repsonse %@" , dict[@"providers"]);
                sCompletion(dict[@"providers"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void) addLocation:(NSString *)name lat:(NSString *)lat long:(NSString *)longi onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"user_id" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID] ,
                                      @"name" : name,
                                      @"longitude" : longi,
                                      @"latitude" : lat
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , ADD_LOCATION] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else if([responseObject[@"message"] isEqualToString:@"success"]){
                NSDictionary *dict = responseObject;
                sCompletion(dict[@"message"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void) addRating:(NSString *)rating comment:(NSString *)comment provider:(NSString *)provider onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"rating" :  rating,
                                      @"comment" : comment,
                                      @"seeker" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID],
                                      @"provider" : provider
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , ADD_RATING] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else if([responseObject[@"message"] isEqualToString:@"success"]){
                NSDictionary *dict = responseObject;
                sCompletion(dict[@"message"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}


+ (void) getLocationsOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"user_id" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID]
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , GET_LOCATIONS] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else {
                NSDictionary *dict = responseObject;
                NSLog(@"Locations repsonse %@" , dict[@"locations"]);
                sCompletion(dict[@"locations"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}


+ (void) editBank:(NSString *)accountNumber branchCode:(NSString *)branchCode bankName:(NSString *)bankName accName:(NSString *)accName onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"user_id" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID] ,
                                      @"account_number" : accountNumber,
                                      @"branch_code" : branchCode,
                                      @"bank_name" : bankName,
                                      @"account_name" : accName
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , EDIT_BANK] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else if([responseObject[@"message"] isEqualToString:@"success"]){
                NSDictionary *dict = responseObject;
                sCompletion(dict[@"message"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void) editProfile:(NSString *)firstName surName:(NSString *)surName mobile:(NSString *)mobile passport:(NSString *)passport pass:(NSString *)pass onSuccess:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"id" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID] ,
                                      @"firstname" : firstName,
                                      @"lastname" : surName,
                                      @"phone" : mobile,
                                      @"passport" : passport,
                                      @"password" : pass
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , EDIT_PROFILE] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else if([responseObject[@"message"] isEqualToString:@"success"]){
                NSDictionary *dict = responseObject;
                sCompletion(dict[@"message"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void) changeJobStatus:(NSString *)status job:(NSString *)jobId onSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{
                                      @"id" : jobId,
                                      @"status" : status
                                      };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , CHANGE_JOB_STATUS] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else if([responseObject[@"message"] isEqualToString:@"success"]){
                NSDictionary *dict = responseObject;
                NSLog(@"status change repsonse %@" , dict[@"ratings"]);
                sCompletion(dict[@"message"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void) getRatingsOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{@"provider" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID] ,
                                      @"seeker" : @"0" };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , GET_RATINGS] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else {
                NSDictionary *dict = responseObject;
                NSLog(@"Jobs repsonse %@" , dict[@"ratings"]);
                sCompletion(dict[@"ratings"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void)getBanksOnSuccess:(void (^)(NSArray *))sCompletion onFailure:(void (^)(NSString *))fCompletion {
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{};
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , GET_BANKS] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else {
                NSDictionary *dict = responseObject;
                NSLog(@"Banks repsonse %@" , dict[@"banks"]);
                sCompletion(dict[@"banks"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void) getProviderJobsOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{@"provider" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID] ,
                                      @"seeker" : @"0" };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , GET_JOBS] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else {
                NSDictionary *dict = responseObject;
                NSLog(@"Jobs repsonse %@" , dict[@"jobs"]);
                sCompletion(dict[@"jobs"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
    
}

+ (void) getSeekerJobsOnSuccess:(void(^)(NSArray *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *requestData = @{@"provider" : @"0" ,
                                      @"seeker" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID] };
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , GET_JOBS] parameters:requestData error:nil];
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
                fCompletion(error.description);
            } else {
                NSDictionary *dict = responseObject;
                NSLog(@"Seeker Jobs repsonse %@" , dict[@"jobs"]);
                sCompletion(dict[@"jobs"]);
            }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
}

+ (void) login:(NSString *)email andPassword:(NSString *)password andType:(NSString *)type onSuccess:(void(^)(NSDictionary *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        NSString *token = @"";
        if([[NSUserDefaults standardUserDefaults] stringForKey:@"mytoken"]) {
            token = [[NSUserDefaults standardUserDefaults] stringForKey:@"mytoken"];
        }
        
        
        NSDictionary *requestData = @{
                                      @"email" : email ,
                                      @"type" : type ,
                                      @"password" : password,
                                      @"token" : token
                                    };

        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        manager.responseSerializer = [AFJSONResponseSerializer
                                      serializerWithReadingOptions:NSJSONReadingAllowFragments];
          
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
          
        NSMutableURLRequest *request = [serializer requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , LOGIN] parameters:requestData error:nil];
          
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
          if (error) {
              NSLog(@"Error: %@", error);
              fCompletion(error.description);
          } else {
              NSDictionary *dict = responseObject;
              NSLog(@"Login repsonse %@" , dict);
              
              if([dict[@"message"] isEqualToString:@"success"])  {
                  
                  [[NSUserDefaults standardUserDefaults] setBool:true forKey:USER_LOGIN];
                  [[NSUserDefaults standardUserDefaults] setObject:type forKey:USER_LOGIN_TYPE];
                  [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Id"] forKey:USER_LOGIN_ID];
                  [[NSUserDefaults standardUserDefaults] setObject:password forKey:USER_LOGIN_PASSWORD];
                  [[NSUserDefaults standardUserDefaults] setObject:email forKey:USER_LOGIN_EMAIL];
                  [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Firstname"] forKey:USER_LOGIN_F_NAME];
                  [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Lastname"] forKey:USER_LOGIN_L_NAME];
                  [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Photo"] forKey:USER_LOGIN_IMAGE];
                  [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"AverageRating"] forKey:USER_LOGIN_AVERAGE_RATINGS];
                  
                  [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Balance"] forKey:USER_LOGIN_BALANCE];
                  
                  [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Phone"] forKey:USER_LOGIN_MOBILE];
                  [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Passport"] forKey:USER_LOGIN_PASSPORT];
                  
                  if([[dict[@"User"][@"Bank"] allKeys] containsObject:@"account_name"]) {
                      [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Bank"][@"account_number"] forKey:USER_LOGIN_BANK_ANUMBER];
                      [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Bank"][@"name"] forKey:USER_LOGIN_BANK_NAME];
                      [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Bank"][@"branch_code"] forKey:USER_LOGIN_BANK_CODE];
                      [[NSUserDefaults standardUserDefaults] setObject:dict[@"User"][@"Bank"][@"account_name"] forKey:USER_LOGIN_BANK_UNAME];
                  }
                  
                  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_LOGIN];
                  [[NSUserDefaults standardUserDefaults] synchronize];
                  
                  sCompletion(dict);
              }
              else fCompletion(@"Invalid account");
          }
        }];
        
        [dataTask resume];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
    }
    @finally {
        
    }
    
}

+ (NSURLSessionUploadTask *) signupProvider:(NSString *)email andPassword:(NSString *)password firstname:(NSString*)fName lastname:(NSString*)lName phone:(NSString*)phone passport:(NSString*)passport image:(NSData *)image accountNumber:(NSString *)accountNumber branch:(NSString *)branchCode bankName:(NSString *)bankName accountName:(NSString *)accountName lat:(NSString *)latitude long:(NSString *)longitude address:(NSString *)address rate:(NSString *)rate radius:(NSString *)radius toDate:(NSString *)toDate fromDate:(NSString *)fromDate passImage:(NSData *)passImage onProgressUpdate:(void(^)(double progress))pUpdate onSuccess:(void(^)(NSDictionary *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        NSString *token = @"";
        if([[NSUserDefaults standardUserDefaults] stringForKey:@"mytoken"]) {
            token = [[NSUserDefaults standardUserDefaults] stringForKey:@"mytoken"];
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *parameters = @{
                                     @"email" : email ,
                                     @"password" : password ,
                                     @"firstname" : fName ,
                                     @"lastname" :  lName,
                                     @"phone" : phone ,
                                     @"passport" : passport ,
                                     @"type" : @"0",
                                     @"account_number" : accountNumber,
                                     @"branch_code" : branchCode,
                                     @"bank_name" : bankName,
                                     @"account_name" : accountName,
                                     @"latitude" : latitude,
                                     @"longitude" : longitude,
                                     @"address" : address,
                                     @"rate" : rate,
                                     @"radius" : radius,
                                     @"to" : toDate,
                                     @"from" : fromDate,
                                     @"token" : token
                                     };
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                                                  URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , SIGNUP]
                                                                                                 parameters:parameters
                                                                                  constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                                                      [formData appendPartWithFileData:image
                                                                                                                  name:@"image"
                                                                                                              fileName:@"image.jpg"
                                                                                                              mimeType:@"image/jpg"];
                                                                                      
                                                                                      [formData appendPartWithFileData:passImage
                                                                                                                  name:@"passport"
                                                                                                              fileName:@"passport.jpg"
                                                                                                              mimeType:@"image/jpg"];
                                                                                      
                                                                                  } error:nil];
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          dispatch_async( dispatch_get_main_queue(), ^{
                              pUpdate(uploadProgress.fractionCompleted);
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          dispatch_async( dispatch_get_main_queue(), ^{
                              if (error) {
                                  NSLog(@"Error: %@", error);
                                  fCompletion(error.description);
                              } else if([responseObject[@"message"] isEqualToString:@"success"]){
                                  NSDictionary *dict = responseObject;
                                  NSLog(@"signup seeker repsonse %@" , dict);
                                  sCompletion(dict);
                              }

                          });
                      }];
        
        [uploadTask resume];
        return uploadTask;
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
        return nil;
    }
    @finally {
        
    }
}

+ (NSURLSessionUploadTask *) signupSeeker:(NSString *)email andPassword:(NSString *)password firstname:(NSString*)fName lastname:(NSString*)lName phone:(NSString*)phone passport:(NSString*)passport image:(NSData *)image lat:(NSString *)latitude long:(NSString *)longitude address:(NSString *)address locName:(NSString *)locName onProgressUpdate:(void(^)(double progress))pUpdate onSuccess:(void(^)(NSDictionary *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        NSString *token = @"";
        if([[NSUserDefaults standardUserDefaults] stringForKey:@"mytoken"]) {
            token = [[NSUserDefaults standardUserDefaults] stringForKey:@"mytoken"];
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NSDictionary *parameters = @{
                                     @"email" : email ,
                                     @"password" : password ,
                                     @"firstname" : fName ,
                                     @"lastname" :  lName,
                                     @"phone" : phone ,
                                     @"passport" : passport ,
                                     @"type" : @"1",
                                     @"latitude" : latitude,
                                     @"longitude" : longitude,
                                     @"loc_name" : locName,
                                     @"token" : token
                                     };
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                                                  URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , SIGNUP]
                                                                                                 parameters:parameters
                                                                                  constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                                                      [formData appendPartWithFileData:image
                                                                                                                  name:@"image"
                                                                                                              fileName:@"image.jpg"
                                                                                                              mimeType:@"image/jpg"];
                                                                                  } error:nil];
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          dispatch_async( dispatch_get_main_queue(), ^{
                              pUpdate(uploadProgress.fractionCompleted);
                          });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          dispatch_async( dispatch_get_main_queue(), ^{
                              if (error) {
                                  NSLog(@"Error: %@", error);
                                  fCompletion(error.description);
                              } else if([responseObject[@"message"] isEqualToString:@"success"]){
                                  NSDictionary *dict = responseObject;
                                  NSLog(@"Login repsonse %@" , dict[@"message"]);
                                  sCompletion(dict);
                              }
                              
                          });
                      }];
        
        [uploadTask resume];
        return uploadTask;
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
        return nil;
    }
    @finally {
        
    }
    
    
}

+ (NSURLSessionUploadTask *) uploadPhoto:(NSData *)image onProgressUpdate:(void(^)(double progress))pUpdate onCompletion:(void(^)(NSString *))sCompletion onFailure:(void(^)(NSString *))fCompletion{
    @try {
        
         NSDictionary *parameters = @{
                                      @"user_id" : [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_ID]
                                      };

        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                          URLString:[NSString stringWithFormat:@"%@%@" , SERVER_URL , UPLOAD_PHOTO]
                         parameters:parameters
          constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
              [formData appendPartWithFileData:image
                                          name:@"image"
                                      fileName:@"image.jpg"
                                      mimeType:@"image/jpg"];
              
        } error:nil];
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                           dispatch_async( dispatch_get_main_queue(), ^{
                               pUpdate(uploadProgress.fractionCompleted);
                           });
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          dispatch_async( dispatch_get_main_queue(), ^{
                              if (error){
                                  fCompletion(error.description);
                              }
                              else {
                                  NSLog(@"Signup response %@",responseObject);
                                  [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"path"] forKey:USER_LOGIN_IMAGE];
                                  [[NSUserDefaults standardUserDefaults] synchronize];
                                  sCompletion(responseObject[@"message"]);
                              }
                          });
                      }];
        
        [uploadTask resume];
        return uploadTask;
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        fCompletion(exception.description);
        return nil;
    }
    @finally {
        
    }
}
@end
