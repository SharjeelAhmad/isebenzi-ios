//
//  BanksTableViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 08/08/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "BanksTableViewController.h"

@interface BanksTableViewController ()

@end

@implementation BanksTableViewController {
    NSArray *banks;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    banks = @[@"NEDBANK" , @"ABSA" , @"BIDVEST BANK" , @"CAPITEC BANK" , @"FIRST NATIONAL BANK" , @"INVESTEC BANK LIMITED" , @"STANDARD BANK OF SOUTH AFRICA" , @"AFRICAN BANK" , @"ALBARAKA BANK" , @"BNP PARIPAS" , @"CITIBANK" , @"GRINDROD BANK" , @"HABIB OVERSEAS BANK LIMITED" , @"HBZ BANK LIMITED" , @"HSBC BANK PLC" , @"JP MORGAN CHASE" , @"PEOPLES BANK LTD" , @"SA BANK OF ATHENS" , @"SASFIN BANK" , @"UBANK LIMITED"];
    

     self.clearsSelectionOnViewWillAppear = NO;
    
    [self.tableView  reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return banks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.textLabel.text = banks[indexPath.row];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedBank = banks[indexPath.row];
    [self performSegueWithIdentifier:@"goHome" sender:self];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
