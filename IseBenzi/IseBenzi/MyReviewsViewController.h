//
//  MyReviewsViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 16/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyReviewsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
