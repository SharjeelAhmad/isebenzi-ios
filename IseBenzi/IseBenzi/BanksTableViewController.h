//
//  BanksTableViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 08/08/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BanksTableViewController : UITableViewController

@property (strong , nonatomic) NSString* selectedBank;

@end
