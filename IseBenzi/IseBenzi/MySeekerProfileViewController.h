//
//  MySeekerProfileViewController.h
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 07/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySeekerProfileViewController : UIViewController <UIImagePickerControllerDelegate , UINavigationControllerDelegate>

@end
