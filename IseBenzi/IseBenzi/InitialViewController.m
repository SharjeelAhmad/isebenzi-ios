//
//  InitialViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 07/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "InitialViewController.h"

@interface InitialViewController ()
@property (weak, nonatomic) IBOutlet UIButton *serviceProvider;
@property (weak, nonatomic) IBOutlet UIButton *seeker;

@end

@implementation InitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _serviceProvider.layer.borderWidth = 1.0f;
    _serviceProvider.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _seeker.layer.borderWidth = 1.0f;
    _seeker.layer.borderColor = [UIColor whiteColor].CGColor;
}

-(void)viewDidAppear:(BOOL)animated {
    if([[NSUserDefaults standardUserDefaults] boolForKey:USER_LOGIN]) {
        if([[[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_TYPE] isEqualToString:@"0"])
            [self goToProvider:nil];
        else
            [self goToSeeker:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any sresources that can be recreated.
}
- (IBAction)goToProvider:(UIButton *)sender {
    [[UINavigationBar appearance] setBarTintColor:NAVIGATION_BACKGROUND_COLOR];
    [self performSegueWithIdentifier:@"goToProvider" sender:self];
}

- (IBAction)goToSeeker:(UIButton *)sender {
    [[UINavigationBar appearance] setBarTintColor:NAVIGATION_BACKGROUND_SEEKER_COLOR];
    [self performSegueWithIdentifier:@"goToSeeker" sender:self];
}

@end
