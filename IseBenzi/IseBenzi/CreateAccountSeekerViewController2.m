//
//  CreateAccountSeekerViewController2.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 07/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "CreateAccountSeekerViewController2.h"

@interface CreateAccountSeekerViewController2 ()
@property (weak, nonatomic) IBOutlet UITextField *type;
@property (weak, nonatomic) IBOutlet UISwitch *visa;
@property (weak, nonatomic) IBOutlet UISwitch *masterCard;
@property (weak, nonatomic) IBOutlet UISwitch *payPal;
@property (weak, nonatomic) IBOutlet UITextField *cardHolerName;
@property (weak, nonatomic) IBOutlet UITextField *cardNumber;
@property (weak, nonatomic) IBOutlet UITextField *cardExpiry;
@property (weak, nonatomic) IBOutlet UITextField *stc;
@property (weak, nonatomic) IBOutlet CustomScroll *scrollView;

@end

@implementation CreateAccountSeekerViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setPlaceHolderTextColor:NAVIGATION_BACKGROUND_SEEKER_COLOR];
    [self.scrollView drawRect:CGRectZero];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)save:(UIButton *)sender {
}

-(void)setPlaceHolderTextColor:(UIColor*)color {
    [_type setValue:color
          forKeyPath:@"_placeholderLabel.textColor"];
    [_cardHolerName setValue:color
            forKeyPath:@"_placeholderLabel.textColor"];
    [_cardNumber setValue:color
                   forKeyPath:@"_placeholderLabel.textColor"];
    [_cardExpiry setValue:color
               forKeyPath:@"_placeholderLabel.textColor"];
    [_stc setValue:color
               forKeyPath:@"_placeholderLabel.textColor"];
}



@end
