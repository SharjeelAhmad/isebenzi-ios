//
//  LocationsViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "LocationsViewController.h"
#import "AddLocationViewController.h"

@interface LocationsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *spinnerView;

@end

@implementation LocationsViewController {
    NSArray *dataSource;
    NSDictionary *selectedLocation;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view bringSubviewToFront:self.spinnerView];
}

-(void)viewWillAppear:(BOOL)animated {
    self.spinnerView.hidden = NO;
    [Server getLocationsOnSuccess:^(NSArray *locs) {
        dataSource = locs;
        [self.tableView reloadData];
        self.spinnerView.hidden = YES;
    } onFailure:^(NSString *err) {
        self.spinnerView.hidden = YES;
    }];
    selectedLocation = nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = dataSource[indexPath.row][@"name"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedLocation = dataSource[indexPath.row];
    
    [self performSegueWithIdentifier:@"locationDetails" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AddLocationViewController *loc = segue.destinationViewController;
    loc.location = selectedLocation;
}

- (IBAction)addLocation:(UIBarButtonItem *)sender {
    
}

@end
