//
//  SeekerJobDetailViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 24/07/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "SeekerJobDetailViewController.h"
#import "AddRatingViewController.h"
#import <QuartzCore/QuartzCore.h>

#define kPayPalEnvironment PayPalEnvironmentSandbox

@interface SeekerJobDetailViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *complete;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *accept;
@property (weak, nonatomic) IBOutlet UINavigationItem *bar;

@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

@end

@implementation SeekerJobDetailViewController {
    NSArray *labels;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    labels = @[@"Skills required : ", @"Worker name : ", @"Date : " , @"Time : ", @"Address" , @"Rating"];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if([_job[@"status"] isEqualToString:@"1"]) {
        NSMutableArray *buttons =self.bar.rightBarButtonItems.mutableCopy;
        
        [buttons removeObject:_cancel];
        [buttons removeObject:_complete];
        
        self.bar.rightBarButtonItems = buttons;
    }else if([_job[@"status"] isEqualToString:@"2"]) {
        NSMutableArray *buttons =self.bar.rightBarButtonItems.mutableCopy;
        
        [buttons removeObject:_accept];
        
        self.bar.rightBarButtonItems = buttons;
    }else {
        NSMutableArray *buttons =self.bar.rightBarButtonItems.mutableCopy;
        
        [buttons removeAllObjects];
        
        self.bar.rightBarButtonItems = buttons;
    }
    
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = NO;
    _payPalConfig.merchantName = @"Isebenzi";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];

    // Setting the languageOrLocale property is optional.
    //
    // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
    // its user interface according to the device's current language setting.
    //
    // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
    // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
    // to use that language/locale.
    //
    // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    // Setting the payPalShippingAddressOption property is optional.
    //
    // See PayPalConfiguration.h for details.
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setPayPalEnvironment:self.environment];
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return labels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = labels[indexPath.row];
    
    if(indexPath.row == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@" ,cell.textLabel.text , self.job[@"occupation"]];
    } else if(indexPath.row == 1) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@" , cell.textLabel.text , self.job[@"provider"]];
    }else if(indexPath.row == 2) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@" ,  cell.textLabel.text , [self.job[@"from_date"] componentsSeparatedByString:@" "].firstObject];
    }else if(indexPath.row == 3) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@"   , cell.textLabel.text , [self.job[@"from_date"] componentsSeparatedByString:@" "].lastObject];
    }
    
    return cell;
}

- (IBAction)acceptJob:(UIBarButtonItem *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIAlertController class]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title" message:@"text mssg" preferredStyle:UIAlertControllerStyleAlert];
            alert.title = [NSString stringWithFormat:@"%@ has accepted and confirmed job!" , self.job[@"provider"]];
            alert.message = [NSString stringWithFormat:@"An amount of $%@ will be deducted from you account." , self.job[@"per_day"]];
            
            UIAlertAction* cancel =  [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            UIAlertAction* ok =  [UIAlertAction actionWithTitle:@"Complete Booking" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                [self pay:self.job[@"per_day"]];

            }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:^{
            }];
        } else {
        }
    });
}

-(void) pay:(NSString *)money {
    // Remove our last completed payment, just for demo purposes.
    self.resultText = nil;
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    // Optional: include multiple items
    PayPalItem *item1 = [PayPalItem itemWithName:@"Job"
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:money]
                                    withCurrency:@"USD"
                                         withSku:@""];
   
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Job Payment";
    payment.items = items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    
    [Server changeJobStatus:@"2" job:self.job[@"id"] onSuccess:^(NSArray *dict) {
        [self.navigationController popViewControllerAnimated:YES];
    } onFailure:^(NSString *error) {
        [Common showAlertWithTitle:@"Oops" message:error self:self];
    }];
    
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
}

#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}

#pragma mark PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
    NSLog(@"PayPal Future Payment Authorization Canceled");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}

- (IBAction)closeJob:(UIBarButtonItem *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIAlertController class]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title" message:@"text mssg" preferredStyle:UIAlertControllerStyleAlert];
            alert.title = @"Confirmation";
            alert.message = [NSString stringWithFormat:@"Please confirm that %@ has arrived" , self.job[@"provider"]];
            
            UIAlertAction* cancel =  [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            
            UIAlertAction* ok =  [UIAlertAction actionWithTitle:@"Yes candidate has arrived!" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [Server changeJobStatus:@"3" job:self.job[@"id"] onSuccess:^(NSArray *dict) {
                    [self performSegueWithIdentifier:@"ratings" sender:self];
                } onFailure:^(NSString *error) {
                    [Common showAlertWithTitle:@"Oops" message:error self:self];
                }];
            }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            
            [self presentViewController:alert animated:YES completion:^{
            }];
        } else {
        }
    });
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AddRatingViewController *rating = segue.destinationViewController;
    rating.provider = self.job[@"provider"];
    rating.provideId = self.job[@"provider_id"];
}
@end
