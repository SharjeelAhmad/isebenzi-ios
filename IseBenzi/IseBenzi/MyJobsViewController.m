//
//  MyJobsViewController.m
//  IseBenzi
//
//  Created by Sharjeel Ahmad on 18/06/2017.
//  Copyright © 2017 Takhleeq MacBookPro. All rights reserved.
//

#import "MyJobsViewController.h"
#import "JobDetailsViewController.h"

@interface MyJobsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *dataSource;
@property (strong, nonatomic) NSArray *mainSource;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cashoutButton;


@end

@implementation MyJobsViewController {
    //NSString *status;
    NSDictionary *selectedJob;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //status = @"0";
    [Common setNavigationTitleFont:self.navigationController];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [Server getProviderJobsOnSuccess:^(NSArray *array) {
        self.mainSource = array;
        
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"status contains[cd] %@", @"0"];
        self.dataSource = [self.mainSource filteredArrayUsingPredicate:bPredicate];
        
        [self.tableView reloadData];
    } onFailure:^(NSString *error) {
        
    }];
    
    self.cashoutButton.title = [NSString stringWithFormat:@"CASH OUT(%@$)" , [[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_BALANCE]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *name = [cell viewWithTag:1];
    UILabel *date = [cell viewWithTag:2];
    
    name.text = self.dataSource[indexPath.row][@"seeker"];
    date.text = self.dataSource[indexPath.row][@"from_date"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedJob = self.dataSource[indexPath.row];
    [self performSegueWithIdentifier:@"showJob" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    JobDetailsViewController *jobDetails = segue.destinationViewController;
    jobDetails.job = selectedJob;
}

- (IBAction)requestPaymeny:(UIBarButtonItem *)sender {
    [Server requestPayment:^(NSArray *msg) {
        [Common showAlertWithTitle:@"Request" message:@"Successfull" self:self];
    } onFailure:^(NSString *error) {
        [Common showAlertWithTitle:@"Oops" message:@"Try again" self:self];
    }];
}


- (IBAction)segment:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0: {
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"status contains[cd] %@", @"0"];
            self.dataSource = [self.mainSource filteredArrayUsingPredicate:bPredicate];
            
            [self.tableView reloadData];
            
            break;

        }
            
        case 1: {
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"status contains[cd] %@", @"2"];
            self.dataSource = [self.mainSource filteredArrayUsingPredicate:bPredicate];
            
            [self.tableView reloadData];
            
            break;
            
        }
            
        case 2: {
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"status contains[cd] %@", @"3"];
            self.dataSource = [self.mainSource filteredArrayUsingPredicate:bPredicate];
            
            [self.tableView reloadData];
            
            break;
            
        }
        default:
            break;
    }
}

@end
