//
//  ViewController.m
//  IseBenzi
//
//  Created by Sharjeel MacBookPro on 24/05/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIImageView *emailCirculeImage;
@property (weak, nonatomic) IBOutlet UIImageView *passwordCiculeImage;
@property (weak, nonatomic) IBOutlet CustomScroll *scrollView;
    @property (weak, nonatomic) IBOutlet UIView *spinnerView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBorderColor:_email];
    [self setBorderColor:_password];
    [Common setNavigationTitleFont:self.navigationController];
    [self roundImageView:_emailCirculeImage];
    [self roundImageView:_passwordCiculeImage];
    [_email setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    [_password setValue:[UIColor whiteColor]
          forKeyPath:@"_placeholderLabel.textColor"];

    [self.scrollView drawRect:CGRectZero];
    
    [self.view bringSubviewToFront:self.spinnerView];
}

-(void)viewDidAppear:(BOOL)animated {
    if([[NSUserDefaults standardUserDefaults] boolForKey:USER_LOGIN]) {
        self.spinnerView.hidden = NO;
        [Server login:[[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_EMAIL] andPassword:[[NSUserDefaults standardUserDefaults] stringForKey:USER_LOGIN_PASSWORD] andType:@"0" onSuccess:^(NSDictionary *dict) {
            [self performSegueWithIdentifier:@"goToDashboard" sender:self];
            self.spinnerView.hidden = YES;
        } onFailure:^(NSString *error) {
            [Common showAlertWithTitle:@"Oops" message:error self:self];
            self.spinnerView.hidden = YES;
        }];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setBorderColor:(UITextField*)textField {
    textField.layer.cornerRadius=2.0f;
    textField.layer.masksToBounds=YES;
    textField.layer.borderColor=[[UIColor whiteColor]CGColor];
    textField.layer.borderWidth= 1.0f;
}

-(void)roundImageView:(UIImageView*)img {
    img.layer.cornerRadius = 15;
    img.layer.masksToBounds = YES;
}
    
- (IBAction)forgotPassword:(UIButton *)sender {
}
    
- (IBAction)doLogin:(UIButton *)sender {
    if([_email.text isEqualToString:@""] || [_password.text isEqualToString:@""]) {
        [Common showAlertWithTitle:@"Oops" message:@"All fields required" self:self];
    }else {
        self.spinnerView.hidden = NO;
        [Server login:_email.text andPassword:_password.text andType:@"0" onSuccess:^(NSDictionary *dict) {
            [self performSegueWithIdentifier:@"goToDashboard" sender:self];
            self.spinnerView.hidden = YES;
        } onFailure:^(NSString *error) {
            [Common showAlertWithTitle:@"Oops" message:error self:self];
            self.spinnerView.hidden = YES;
        }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self doLogin:nil];
    return true;
}
    
- (IBAction)createAccount:(UIButton *)sender {
}

- (IBAction)backTapped:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:true completion:^{
    }];
}

@end
