//
//  CreateAccountViewController3.m
//  IseBenzi
//
//  Created by Apple on 10/06/2017.
//  Copyright © 2017 Sharjeel MacBookPro. All rights reserved.
//

#import "CreateAccountViewController3.h"

@interface CreateAccountViewController3 ()
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UILabel *fromDate;
@property (weak, nonatomic) IBOutlet UILabel *fromTime;
@property (weak, nonatomic) IBOutlet UILabel *fromAm;
@property (weak, nonatomic) IBOutlet UILabel *toTime;
@property (weak, nonatomic) IBOutlet UILabel *toAm;
@property (weak, nonatomic) IBOutlet UILabel *toDate;

@property (weak, nonatomic) IBOutlet UITextField *dailyRate;
@property (weak, nonatomic) IBOutlet UITextField *location;
@property (weak, nonatomic) IBOutlet UITextField *address;
    @property (weak, nonatomic) IBOutlet UIView *spinnerView;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation CreateAccountViewController3 {
    NSDate *dateFrom;
    NSDate *dateTo;
    CLLocationCoordinate2D selectedLocation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    
    dateTo = [NSDate date];
    dateFrom = [NSDate date];
    
    [self.view bringSubviewToFront:self.spinnerView];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer  alloc] initWithTarget:self action:@selector(longpressToGetLocation:)];
    
    lpgr.minimumPressDuration = 0.5;
    lpgr.delaysTouchesBegan = true;
    lpgr.delegate = self;
    
    [self.mapView addGestureRecognizer:lpgr];
}

- (void)longpressToGetLocation:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    MKMapView* view = (MKMapView*) gestureRecognizer.view;
    //CGPoint loc = [gestureRecognizer locationInView:view];
    //MKNewAnnotationContainerView* subview = (MKNewAnnotationContainerView *) [view hitTest:loc withEvent:nil];
    
    CGPoint touchPoint = [gestureRecognizer locationInView:view];
    selectedLocation = [view convertPoint:touchPoint toCoordinateFromView:view];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = selectedLocation;
    for (id annotation in view.annotations) {
        [view removeAnnotation:annotation];
    }
    [view addAnnotation:point];
}

- (IBAction)availabilityFrom:(UIButton *)sender {
    [_datePickerView setHidden:NO];
    _datePicker.tag = 1;
}

- (IBAction)availabilityTo:(UIButton *)sender {
    [_datePickerView setHidden:NO];
    _datePicker.tag = 2;
}

- (IBAction)hideDatePicker:(UIButton *)sender {
    [_datePickerView setHidden:YES];
}

- (IBAction)createAccount:(UIButton *)sender {
    if([_dailyRate.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Rate required" self:self];
    else if([_dailyRate.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Location required" self:self];
    else if([_address.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Address required" self:self];
    else if([_address.text isEqualToString:@""]) [Common showAlertWithTitle:@"Oops" message:@"Address required" self:self];
    else if(!dateTo) [Common showAlertWithTitle:@"Oops" message:@"From date required" self:self];
    else if(!dateFrom) [Common showAlertWithTitle:@"Oops" message:@"To date required" self:self];
    else if(selectedLocation.latitude == 0) [Common showAlertWithTitle:@"Oops" message:@"Map Location required" self:self];
    
    else {
        self.spinnerView.hidden = NO;
        
        NSDateFormatter *gmtDateFormatter = [[NSDateFormatter alloc] init];
        gmtDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        
        [Server signupProvider:self.email andPassword:self.password firstname:self.firstName lastname:self.surName phone:self.mobileNum passport:self.passportNum image:UIImageJPEGRepresentation(self.userImageView, 0.6) accountNumber:self.accountNumber branch:self.bankCode bankName:self.bankName accountName:self.accountHolderName lat:[NSString stringWithFormat:@"%f", selectedLocation.latitude] long:[NSString stringWithFormat:@"%f", selectedLocation.longitude] address:self.address.text rate:self.dailyRate.text radius:[NSString stringWithFormat:@"%f",self.mapView.region.span.latitudeDelta] toDate:[gmtDateFormatter stringFromDate:dateTo] fromDate:[gmtDateFormatter stringFromDate:dateFrom] passImage:UIImageJPEGRepresentation(self.passImageView, 0.6) onProgressUpdate:^(double progress) {
        } onSuccess:^(NSDictionary *success) {
            [Server login:self.email andPassword:self.password andType:@"0" onSuccess:^(NSDictionary *dict) {
                [self performSegueWithIdentifier:@"goToDashboard" sender:self];
                self.spinnerView.hidden = YES;
            } onFailure:^(NSString *error) {
                [Common showAlertWithTitle:@"Oops" message:@"Login Failed" self:self];
                self.spinnerView.hidden = YES;
            }];
        } onFailure:^(NSString *error) {
            self.spinnerView.hidden = YES;
            [Common showAlertWithTitle:@"Oops" message:error self:self];
        }];
    }
}

- (IBAction)datePickerChange:(UIDatePicker *)sender forEvent:(UIEvent *)event {
    switch (sender.tag) {
        case 1: {
            dateFrom = sender.date;
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            
            [formatter setDateFormat:@"d/MM/YY"];
            self.fromDate.text = [formatter stringFromDate:dateFrom];
            
            [formatter setDateFormat:@"hh꞉mm"];
            self.fromTime.text = [formatter stringFromDate:dateFrom];
            
            [formatter setDateFormat:@"a"];
            self.fromAm.text = [formatter stringFromDate:dateFrom];
            
            break;
        }
        case 2: {
            dateTo = sender.date;
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            
            [formatter setDateFormat:@"d/MM/YY"];
            self.toDate.text = [formatter stringFromDate:dateFrom];
            
            [formatter setDateFormat:@"hh꞉mm"];
            self.toTime.text = [formatter stringFromDate:dateFrom];
            
            [formatter setDateFormat:@"a"];
            self.toAm.text = [formatter stringFromDate:dateFrom];
            
            break;
        }
        default:
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)slider:(UISlider *)sender {
    [self.mapView setRegion:MKCoordinateRegionMake(self.mapView.region.center, MKCoordinateSpanMake(sender.value, sender.value))];
}


@end
