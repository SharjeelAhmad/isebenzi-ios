//
//  ParentViewController.h
//  LeoMarchent
//
//  Created by Sharjeel MacBookPro on 22/07/2016.
//  Copyright © 2016 Sharjeel MacBookPro. All rights reserved.
//

#import "JASidePanelController.h"



@protocol NavigationMenuDelegate <NSObject>

-(void)toggleLeftMenu;
-(void)refreshLeftScreen;
-(void)toggleProfileScreen;
-(void)toggleHomeScreen;
-(void)logout;

-(void)toggleHomeScreenWithArray:(NSArray*)recipes;
-(void)toggleJobsScreen;
-(void)toggleAboutScreen;
-(void)toggleInsertScreen;
-(void)toggleFavouriteScreen;
-(void)toggleProductScreen;
-(void)changeLanguage;

@end


@interface ParentViewController : JASidePanelController <NavigationMenuDelegate>
@end
